import React, { Component } from 'react';
import './CSS/style.css';

{/* Footer 	
	Use -> import Footer from './Pages/Footer.js'
	Then call it with -> <Footer /> */}
class Footer extends Component { 
	render() {
	return (
        <div className="container">
          <footer className="footer">
            This is the current footer.
            <br />
            Will fill this with something at some point.
			Check out the 
			<a href="https://gitlab.com/shawnbutler/ontime">
			gitlab 
			</a>
			which is currently private.
          </footer>
        </div>
		);
	}
}

export default Footer;