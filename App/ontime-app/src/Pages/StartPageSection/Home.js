import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/style.css';

class Home extends Component {
	componentDidMount() {
		localStorage.setItem('userId', '');
	}
	render() {
	return (
		<div>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Site name pending?</title>
        <link rel="stylesheet" href="style.css" />
        {/* First title/header and links*/}
        <section className="container">
          <nav>
            {/* I feel like this might turn into Ontime or a logo?
			Maybe more links I don't know yet */}
            <div className="titles">Welcome to the work in progress Homepage</div>
            <div className="mainLinks">
              <ul>
                {/* Should be very easy to add more links here */}
                <li><a href><Link to='/'>Home</Link></a></li>
                <li><a href><Link to='/SignIn'>Sign in</Link></a>
                  {/* Maybe work on login dropdown/popup at a different point in time.*/}
                </li>
              </ul>
            </div>
          </nav>
        </section>
        {/* Second title */}
        <div className="container">
          <div className="indent">
            <div className="titles">Hmm?</div>
          </div>
        </div>
        {/* Textbox 1*/}
        <div className="container">
          <div className="semititle">A title</div>
          <div className="textbox">
            This is some explanation text maybe?
            <br />
            I'll just add more breaks.
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </div>
        </div>
        {/* Footer */}	
        <div className="container">
          <footer className="footer">
            This is a footer.
            <br />
            Will fill this with something at some point.
          </footer>
        </div>
		</div>
	)
	}
}

export default Home;