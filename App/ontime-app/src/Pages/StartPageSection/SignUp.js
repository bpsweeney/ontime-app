import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/style.css';
import axios from 'axios';
import http from '../../http-common';

class SignUp extends Component {
	constructor(props){
		super(props);
			this.state = {
				Users: {}
			};
	}
   	componentDidMount(){
		http.get()
		.then(res => res.data)
		.then(user => {
			this.setState({
			// I think our best bet is to send the entire DB
			// and store it in data.
			// This is kind of my fault for how I made the frontend.
				Users: user.users
			});
		});
	} 
	render() {
	if(this.state.Users != null){
		console.log(this.state.Users)
		var check = this.state.Users
		console.log(check)
	}
	{var val = "INVALID";}
	function send(e){
		e.preventDefault();
		const SignUpF = document.getElementById("SignUpForm");
		const username = SignUpF.name.value;
		const pass = SignUpF.pass.value;
		const passcon = SignUpF.passcon.value;
		const email = SignUpF.email.value;
		const loginE = document.getElementById("loginError");
		// Add actual email checking later
		if(email == ""){
			loginE.style.opacity = 1;
		}
		// No name.
		else if(username == ""){
			loginE.style.opacity = 1;
		}
		// No duplicate names?
		//for now
		else if(false){
			loginE.style.opacity = 1;
		}
		//Fill out password fields 
		else if(pass == "" || passcon == ""){
			loginE.style.opacity = 1;
		}
		//Fields do not match
		else if(pass != passcon){
			loginE.style.opacity = 1;
		}
		else if(check == null){
			//Error state
		}
		// Add more stuff to the form if you want.
		else{
			var n = 96;
			var i = 0;
			var a = [];
			for(i; i < n; i++){
				a.push(false);				
			}
			var id = check.length + 1
			var Testdata = {
				"id": id,
				"username": username,
				"email": email,
				"img":"https://robohash.org/voluptatemcumipsum.png?size=150x150&set=set1", //Hardcoding for now
				"color":"#a69a9a",
				"friends": [],
				"Suncal": a,
				"Moncal": a,
				"Tuecal": a,
				"Wedcal": a,
				"Thucal": a,
				"Frical": a,
				"Satcal": a,
				"password": pass
			};
			// I think more needs to be made in the backend.
			console.log(Testdata);
			http.post("",Testdata)
			.then((res) => {
				console.log(res);
			});
			window.location.href = "/SignIn"
/* 			http.post({json: {
				_id: "-039kseofpk0",
				username,
				name,
				email,
				password: pass
			},
			function(error,res,body){
				if(!error && res.statusCode == 200) {
					console.log(body)
				}
			}
			}); */
			//window.location.href = "/SignIn";
		}
	}
	return (
	<div>
	  <meta charSet="utf-8" />
	  <meta name="viewport" content="width=device-width, initial-scale=1" />
	  <title>UsernameVariable? info</title>
	  <link rel="stylesheet" href="style.css" />
	  <section className="container">
		<nav>
		  <div className="mainLinks">
			<ul>
			  {/* Should be very easy to add more links here */}
			  <li><a href><Link to='/'>Home</Link></a></li>
			  <li><a href><Link to='/SignIn'>Sign in</Link></a>
				{/* Maybe work on login dropdown/popup at a different point in time.*/}
			  </li>
			</ul>
		  </div>
		</nav>
	  </section>
	  <div className="containersignup">
		<div className="signupform">
		  <form className="information" id="SignUpForm">
			<h3 className="titleInLogin">Please fill out all fields.</h3>
			<input type="email" className="inputField" name="email" placeholder="EMAIL" required />
			<input type="text" className="inputField" name="name" placeholder="USERNAME" required />
			<input type="password" className="inputField" name="pass" placeholder="PASSWORD" required />
			<input type="password" className="inputField" name="passcon" placeholder="CONFIRM PASSWAORD" required />
			<div className="Error T" id="loginError">
				{val}
			</div>
			<button type="submit" className="loginBtn" id="LoginBtn" onClick={send}>
			  Submit
			</button>
		  </form>
		</div>
	  </div>
	</div>
	)
	}
}

export default SignUp;