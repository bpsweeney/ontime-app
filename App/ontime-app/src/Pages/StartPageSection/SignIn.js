import React, { Component } from 'react';
import {Link, Redirect} from 'react-router-dom';
import '../CSS/style.css';
import http from '../../http-common';
import Axios from 'axios';
//import _ from 'lodash';

class SignIn extends Component {
// I will leave this for the the connector to figure out.
// What I want is for this page to do something like this
// <Link to={{pathname:'/user/'+USER_VAL_FROM_DATABASE/LOGIN, state: { curr_user: USER_VAL_FROM_DATABASE/LOGIN}}}>
// The idea is to send the user to their page and set the state to their user value.
 	constructor(props){
		super(props);
			this.state = {
				Users: null/* http.get()
					.then(res => {
						//console.log(res.data.users);
						return res.data.users;
					})
					.catch(e => {
						return null;
					}),  */
/* 				UN: http.get()
					.then(res => {
						//console.log(res.data.users.username);
						return (res.data.users.username);
					})
					.catch(e => {
						return null;
					}) */
			};
	}
   	componentDidMount(){
		http.get()
		.then(res => res.data)
		.then(user => {
			this.setState({
				Users: user.users
			});
		});
	}   
	render() {
/* 	const {user} = this.state
	if(user != null){
		let Info = _.map(user, (value, prop) => {
			return { "prop": prop, "value": value };
		});
		console.log(user.username)
	} */
{/* const {ref} = this.state;
	if(ref) return <Redirect to={ref} />; */}
	{/*Function from old SignInHelper*/}
	//var val2 = require(this.state.Users.data.json);
	function validate(e){
		e.preventDefault();
		const loginF = document.getElementById("loginForm");
		const username = loginF.username.value;
		const password = loginF.password.value;
		const loginE = document.getElementById("loginError");
		let isValidLogin = true //UsersController.loginCheck(username,password)// returns a true or false
		if(isValidLogin){

		//if(username === "AName" && password === "APassword"){
			alert("Logging in");
			{/* Need a redirect to pass a state around  
			    For now user will set your state as
				spell0. Not going to go around maintaining that state
				because refreshing the page destroys it. 
				Cannot get redirect to produce the same outcomes :/*/}
			//return <Redirect to='/user/'/>
			let PAGE = "/user/";
			//const res = await fetch();
			let id = 8;
			/* This is what will keep track of state */
			/* To see where it is stored go to application then local storage */
			localStorage.setItem('userId', id);
			PAGE = PAGE + id;
			
			window.location.href = PAGE; //This works now but it's probably a bad idea.
			//return(
			//	<div>
			//	{this.changeit}
			//	</div>
			//)
			//return <Redirect to='/user/1' />;
		}
		else{
			//might be an issue for color
			if(loginE.style.color == "blue"){loginE.style.color = "red"}
			else{loginE.style.color = "blue"}
			loginE.style.opacity = 1;
		}
	}
//	const {users} = this.state.Users
//	console.log(this.state.UN);
  	if(this.state.Users != null){
		console.log(this.state.Users);
		console.log(typeof this.state.Users);
		console.log(this.state.Users[1].username);
	}
	return (
		<div>
		  <meta charSet="utf-8" />
		  <meta name="viewport" content="width=device-width, initial-scale=1" />
		  <title>Login</title>
		  <link rel="stylesheet" href="style.css" />
		  <section className="container">
			<nav>
			  <div className="mainLinks">
				<ul>
				  {/* Should be very easy to add more links here */}
				  <li><a href><Link to='/'>Home</Link></a></li>
				  <li><a href><Link to='/SignIn'>Sign in</Link></a>
                  </li>
				</ul>
			  </div>
			</nav>
		  </section>
		  <div className="container">
			<div className="loginBox">
			  <h3 className="titleInLogin">Sign in to proceed</h3>
			  <form id="loginForm" className="information" /*onSubmit={this.handleSubmit}*/>
				<input type="text" className="inputField" name="username" placeholder="USERNAME" 
				/* value={username} onChange={this.handleChange} */ required />
				<input type="password" className="inputField" name="password" placeholder="PASSWORD"
				/* value={password} onChange={this.handleChange} */ required />
				<div className="Error" id="loginError">
				  Incorrect sign in
				</div>
				<a href className="passReset">Forgot your password?</a>
				<button type="submit" className="loginBtn" id="LoginBtn" onClick={validate}>
				  Login
				</button>
			  </form>
			</div>
			<div className="signUpBox">
			  Don't have an account?
			  <Link to='/SignUp'><a className="loginBtn">Sign up</a></Link>
			</div>
		  </div>
		</div>

	)
	}
/* 	handleChange = event => {
	this.setState({
		[event.target.name]: event.target.value
	});
	};
	handleSubmit = event => {	
		if(this.username == "AName" && this.password == "APassword"){
		alert("Logging in");
		return(
			<Link to='/user'></Link>
		)
		}
		else{
			loginE.style.opacity = 1;
		}
	} */
}

export default SignIn;