import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import drop from './userDDcode.js';
import './CSS/style.css';
import './CSS/profile.css';
import Nav from './userNav';
////////////////////////////////////////////////////////////////////////////////
// REMOVE LATER
class user extends Component { 
	constructor(props){
		super(props);
		this.state = {
			curr_user: ''
		};
	}
	render() {
	return (
		<div>
		  <meta charSet="utf-8" />
		  <meta name="viewport" content="width=device-width, initial-scale=1" />
		  <title>Profile</title>
		  <link rel="stylesheet" href="profile.css" />
		  <link rel="stylesheet" href="style.css" />
		  <section className="container">
			<Nav />
		  </section>
		  <div className="profileContainer">
			<div className="titles">User's Profile</div>
			<div className="profileBox">
			  <div className="profileHeader">
				<div className="ornament" />
				{/* Hold the image somewhere in the backend?
						Also need a default image probably.*/}
				<img className="imgItem" width={175} height={175} src="image.png" />
				<div className="name-links">
				  <div className="semititle">
					UserName?
				  </div>
				  <div className="userLinks">
					<span>Links: </span> 
					<a href>
					  A Link
					</a>
					<span>, </span>
					<a href>
					  Another Link
					</a>
				  </div>
				</div>
			  </div>
			  <div className="profilebody">
				<div className="calendarHolder">
				  {/* The link to make your own calendar*/}
				  {/* https://calendar.google.com/calendar/u/1/embedhelper?src=c_dd8c6le2j07qdruajfl517qq04%40group.calendar.google.com&ctz=America%2FAnchorage*/}
				  <iframe src="https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23F09300&ctz=America%2FAnchorage&showTitle=1&showNav=1&showDate=1&title=Test%20Calendar&showPrint=1&showTabs=1&showCalendars=1&showTz=1" style={{border: 'solid 1px #777', width: '100%'}} width={800} height={600} frameBorder={0} scrolling="no">
					&lt;!-- Make sure to add width: 100%; back if you change the calendar --&gt;
				  </iframe>
				</div>
			  </div>
			</div>
		  </div>
		  <br />
		  <br />
		</div>
	)
	}
}

export default user;




