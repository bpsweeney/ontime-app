import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/profile.css';
import '../CSS/style.css';
import '../CSS/group.css';
//import groupData from '../Json/groupData.json';
//import udata from '../Json/friendTest.json';
import Nav from './components/userNav';
import Times from './components/TimesDropdown.js';
import http from '../../http-common';

class createGroup extends Component { 
	constructor(props){
		super(props);
		this.state = {
			curr_user: localStorage.getItem('userId'),
			groups: null,
			users: null
		};
	}
	componentDidMount() {
		http.get("/Groups")
		.then(res => res.data)
		.then(user => {
			this.setState({
				groups: user.users
			});
		});
		http.get("")
		.then(res => res.data)
		.then(user => {
			this.setState({
				users: user.users
			});
		});
	}
	render(){
	if(this.state.groups != null){
		console.log(this.state.groups)
		var groupData = this.state.groups
		console.log(groupData)
	}
	if(this.state.users != null){
		console.log(this.state.users)
		var udata = this.state.users
		console.log(udata)
	}
		function filterByname(data, name){
			return data.filter(function(data){return (data.name == name);})[0];
		}
		function MakeGroup(e){
			e.preventDefault();
			const Groupname = document.getElementById("GroupName");
			const invite = Groupname.Invites.value;
			const Gname = Groupname.GroupName.value;
			var val = filterByname(groupData, Gname)
			console.log(val)
			if(val != null || localStorage.getItem('userId') == "" || localStorage.getItem('userId') == null){
				alert("Name taken");
			}
			else{
				var n = 96;
				var i = 0;
				var a = [];
				for(i; i < n; i++){
					a.push(false);
				}
				var invites = [];
				var musers = [];
				musers = invite.split(" ");
				var returneduser = 0;
				for(let m = 0; m < musers.length; m++){
					returneduser = filterByname(udata, musers[m])
					if(returneduser != undefined && !invites.includes(returneduser.id)){
						//Can't invite yourself
						if(returneduser.id != localStorage.getItem('userId')){
							invites.push(returneduser.id)
						}
					}
				}
				//console.log(invites)
				var dataForBack = {
					"name": Gname,
					"aboutUs": "",
					"users": [localStorage.getItem('userId')],
					"owner": localStorage.getItem('userId'),
					"PendingInvites": invites,
					"SunMeetTimes": a,
					"MonMeetTimes": a,
					"TueMeetTimes": a,
					"WedMeetTimes": a,
					"ThuMeetTimes": a,
					"FriMeetTimes": a,
					"SatMeetTimes": a
				}
				//dataForBack = JSON.stringify(dataForBack);
				console.log(dataForBack);
				http.post('/MakeGroup',dataForBack)
				.then((res) => {
					console.log(res);
				})
				window.location.href = "/groups"
			}
		}
		return(
			<div>
			<section className="container">
			   <Nav />
		    </section>
			<div className="container">
				<div className="sectionHolder">
					<div className="titles">
						Create A New Group
					</div>
				</div>
				<div className="textbox smaller">
					<div className="semititle"> Group Name </div>
					<form id="GroupName" className="nameInput">
						<input type="text" className="inputField" name="GroupName" placeholder="Group Name" required/>
						{/*Feels like the simplest way to do it.*/}
						<div className="semititle textFix">Invite users</div>
						<input type="text" className="inputField" name="Invites" placeholder="Space separated list of initial users to invite" />
						Meeting times can be made once your group is made.<br/> Same with adding other users?<br/>
						<div className="Extra">
						<button type="submit" className="makeGroupBtn" onClick={MakeGroup}>
							Create Group
						</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		)
	}
}

export default createGroup;