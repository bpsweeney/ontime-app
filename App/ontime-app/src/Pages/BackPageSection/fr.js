import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/profile.css';
import '../CSS/style.css';
import data from '../Json/friendTest.json';
import Nav from './components/userNav';

class fr extends Component {
	constructor(props){
		super(props);
		this.state = {
			curr_user: localStorage.getItem('userId')
		};
	}
	render() {
	return(
		<div>
			<section className="container">
			   <Nav />
		    </section>
			<div className="container">
			<div className="friendBox">
				<h2 className="semititle">Friends</h2>
			<div className="useritems">
			{/*The idea is every user with curr_user in friend list
			   will be put on the current user's friend list.*/}
			{/*Based on the user's state this page will look different.*/}
			{data.filter((val)=>{
				if(val.friends.includes(Number(this.state.curr_user))) {
					return val
				}
				else{
					return null
				}
			})
			.map((val, key)=> {
				return (
					<div className="userBox" key={key}>
						<img className="imgItem" width={100} height={100} src={val.img} />
						<Link to={'/user/'+ val.id}>
						<a href className="FriendLink">
							{val.name}
						</a>
						</Link>
					</div>
				);
			})
			}
			</div>
			</div>
			</div>
		</div>
	);
	}
}

export default fr;