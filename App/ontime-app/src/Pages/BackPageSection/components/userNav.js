import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import drop from './userDDcode.js';
import '../../CSS/style.css';
import '../../CSS/profile.css';
//This is the navBar for all main website pages.

class userNav extends Component { 
	render() {
	return (
		<nav>
			<div className="userlinks">
			  <span className="menuOpener" id="menu" onClick={drop}>Your profile</span>
				<ul className="userMenu" id="dropdown">
				  <li className="item">
				  {/* Not going to go through and do this to everything because refreshing any other page breaks this*/}
				  {/*<Link to={{pathname:'/user/'+this.state.curr_user, state: { curr_user: this.state.curr_user}}}>
					  Navbar has no curr_user I'm guess I might be able to pass it in though. */}
					  {/*In favor of passing 1 as current state from Nav user for now.*/}
					  {/*Found a bug.  If you try to click the link of the page you are on
					     it won't reload the page.  This is very clear when you click my
						 page on someone elses page. */}
				  <Link to={{pathname:'/user/'+localStorage.getItem('userId')}}>
					<a className="Link" href>
					  My page
					</a>
				  </Link>
				  </li>
				  <li className="item">
				  <Link to='/groups'>
					<a className="Link" href>
					  Groups
					</a>
				  </Link>
				  </li>
				  <li className="item">
				  <Link to='/Invites'>
					<a className="Link" href>
					  Group Invites
					</a>
				  </Link>
				  </li>
				  <li className="item">
				  <Link to='/Editprofile'>
					<a className="Link" href>
					  Edit profile
					</a>
				  </Link>
				  </li>
				  <li className="item">
				  <Link to='/friends'>
					<a className="Link" href>
					  Friends
					</a>
				  </Link>
				  </li>
				  <li className="item">
				  <Link to='/'>
					<a className="Link" href>
					  Sign out
					</a>
				  </Link>
				  </li>
				</ul>
			  </div>
			  {/* End of user links section */}
			  <div className="mainLinks">
				<ul>
				  <li><Link to='/usersearch'><a href>User Search</a></Link>
					{/* This needs some logic to change based on a token of signed in or not.*/}
					{/* To be simple for now it's just this */}
				  </li><li><Link to='/'><a href>Sign out</a></Link>
				  </li>
				</ul>
			  </div>
			</nav>
		);
	}
}

export default userNav;