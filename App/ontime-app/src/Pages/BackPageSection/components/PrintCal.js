import React, { Component } from 'react';
import '../../CSS/style.css';
import '../../CSS/profile.css';
import data from '../../Json/friendTest.json';

class Cal extends Component { 
	constructor(props){
		super(props);
		this.state = {
			// Will figure this out.
			userId: localStorage.getItem('userId')
		};
	}
	render() {
	function filterById(data, id){
		return data.filter(function(data){return (data.id == id);})[0];
	}
	var calendar = [<div className="first fixWid">Sun</div>,<div className="fixWid">Mon</div>,<div className="fixWid">Tue</div>,<div className="fixWid">Wed</div>,<div className="fixWid">Thu</div>,<div className="fixWid">Fri</div>,<div className="fixWid">Sat</div>]
	{/* Key variable is val */}
	var val = filterById(data, this.state.userId)
	var lis = [];
	var returnVals = [];
	var i = 100;
	var apptext= "AM";
	for(var j = 0; i < 2501; j++){
		var m = 0;
		if(i > 1245){
			apptext = "PM";
			m = i - 1200;
		}
		else{
			m = i;
		}
		if(i >= 2500){
			apptext= "1:00AM";
			m = 0;
		}
		var text = m.toString();
		text = text.replace(/(.{2})$/,':$1');
		text = text + apptext;
		lis.push(text);
		if(j == 3){
			i = i + 40;
			j = -1;
		}
		i = i + 15;
	}
	if(val != null){
		var vals = [];
		returnVals = [<div className="removeBG Efixtext">{lis[0]}</div>];
		for(let a = 0; a < val.Moncal.length; a++){
			var attempt = a + 1;
			vals.push(<div className="removeBG Efixtext">{lis[attempt]}</div>)
			if(val.Suncal[a] == true){
				if(lis[attempt].length == 7){
					vals.push(<div className="GreenBox EditTimeBox"></div>);
				}
				else{
					vals.push(<div className="GreenBox Leff EditTimeBox"></div>);
				}
			}else{
				if(lis[attempt].length == 7){
					vals.push(<div className="GrayBox EditTimeBox"></div>);
				}
				else{
					vals.push(<div className="GrayBox Leff EditTimeBox"></div>);
				}
			}
			if(val.Moncal[a] == true){
				//console.log("Pushed a val");
				vals.push(<div className="GreenBox EditTimeBox"></div>);
			}else{
				//console.log("No val");
				vals.push(<div className="GrayBox EditTimeBox"></div>);
			}
			if(val.Tuecal[a] == true){
				vals.push(<div className="GreenBox EditTimeBox"></div>);
			}else{
				vals.push(<div className="GrayBox EditTimeBox"></div>);
			}
			if(val.Wedcal[a] == true){
				vals.push(<div className="GreenBox EditTimeBox"></div>);
			}else{
				vals.push(<div className="GrayBox EditTimeBox"></div>);
			}
			if(val.Thucal[a] == true){
				vals.push(<div className="GreenBox EditTimeBox"></div>);
			}else{
				vals.push(<div className="GrayBox EditTimeBox"></div>);
			}
			if(val.Frical[a] == true){
				vals.push(<div className="GreenBox EditTimeBox"></div>);
			}else{
				vals.push(<div className="GrayBox EditTimeBox"></div>);
			}
			if(val.Satcal[a] == true){
				vals.push(<div className="GreenBox EditTimeBox"></div>);
			}else{
				vals.push(<div className="GrayBox EditTimeBox"></div>);
			}
			returnVals.push(<div className="rowDisplay">{vals}</div>);
			vals = [];
		}
	}
	// Worth some thought if we want to label current date.
	const d = new Date();
	//0-6 day 0Sun- 6Sat
	let day = d.getDay();
	let Calstart = 0;
	if(day == 1){
		Calstart = 1;
	}
	else if(day == 2){
		Calstart = 2;
	}
	else if(day == 3){
		Calstart = 3;
	}
	else if(day == 4){
		Calstart = 4;
	}
	else if(day == 5){
		Calstart = 5;
	}
	else if(day == 6){
		Calstart = 6;
	}
	//console.log(lis)
	//console.log(val.Moncal)
	//console.log(vals)
	return (
		<div className="calHolder">
			Your current calendar.
			<div className="rowDisplay">
				<div className="GreenBox keyBox" /> = Time frame is currently set
				<div className="GrayBox keyBox margLeft" /> = Time frame is not set
			</div>
			<div className="rowDisplay">
				{calendar}
			</div>
			{returnVals}
			{/*val.Moncal.length*/}
			{/* {() => {for(let i = 0;i < arr.length(); i++){
				if(arr[i] == True){
					return(
						<div className="startCalrow">
							
							<div className="Box">
						</div>
					)
				}
				else{
					return(grayBox)
				}
			} */}
		</div>
		);
	}
}

export default Cal;