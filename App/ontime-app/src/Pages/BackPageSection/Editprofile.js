import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/style.css';
import '../CSS/profile.css';
import '../CSS/EditP.css';
import Nav from './components/userNav';
import Times from './components/TimesDropdown.js';
//import updateCal from './components/UpdateUCal.js';
//import Cal from './components/PrintCal.js';
//import data from '../Json/friendTest.json';
import http from '../../http-common';

class Editprofile extends Component {
	constructor(props){
		super(props);
		this.state = {
			curr_user: localStorage.getItem('userId'),
			users: null
		};
	}
	componentDidMount() {
		http.get()
		.then(res => res.data)
		.then(user => {
			this.setState({
				users: user.users
			});
		});
	}
	render() {
	function filterById(data, id){
		return data.filter(function(data){return ( data.id == id);})[0];
	}
	if(this.state.users != null){
		var val = filterById(this.state.users, this.state.curr_user)
	}
	function updateCal(e){
		e.preventDefault();
		let StartVal = document.getElementById("Stimes").value;
		let EndVal = document.getElementById("Etimes").value;
		let SVal = parseFloat(StartVal.replace(/:/g, ''));
		let EVal = parseFloat(EndVal.replace(/:/g, ''));
		//Not needed.
		//SVal = Number(SVal);
		//EVal = Number(EVal);
		if(EVal <= SVal){
			alert("Invalid time frame.");
		}
		else{
			let lis = [];
			let i = 100;
			let state = false;
			for(let j = 0; i < 2501; j++){
				if(i >= SVal && i <= EVal){
					if(state == false){
						state = true;
					}
					else
						lis.push(true);
						state = true
				}
				else{
					lis.push(false);
					state = false;
				}
				if(j == 3){
					i = i + 40
					j = -1
				}
				i = i + 15
			}
			//
			//Make code here to update backend with lis.
			//
			// Based on radio button and
			// Days of the week checked.
			// 
			
			if(lis.length == 97){
				alert("Not valid input.")
			}
			else{
				//console.log(lis);
				if(document.getElementById('r1').checked) {
					console.log("Do a set cal")
				// TT = T, TF = T, FT = T, FF = F
					var out = [];
					if(document.getElementById('Sun').checked){
						console.log("Send to user Sun[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == true || val.Suncal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Sun", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Mon').checked){
						console.log("Send to user Mon[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == true || val.Moncal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Mon", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Tue').checked){
						console.log("Send to user Tue[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == true || val.Tuecal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Tue", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Wed').checked){
						console.log("Send to user Wed[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == true || val.Wedcal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Wed", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Thu').checked){
						console.log("Send to user Thu[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == true || val.Thucal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Thu", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Fri').checked){
						console.log("Send to user Fri[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == true || val.Frical[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Fri", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Sat').checked){
						console.log("Send to user Sat[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == true || val.Satcal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Sat", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
				}
				else{
					console.log("Do a remove")
					var out = [];
					// First array is lis, second one is the current user array.
					// FF = F, FT = T, TF = F, TT = F
					if(document.getElementById('Sun').checked){
						console.log("Send to user Sun[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == false && val.Suncal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Sun", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Mon').checked){
						console.log("Send to user Mon[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == false && val.Moncal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Mon", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Tue').checked){
						console.log("Send to user Tue[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == false && val.Tuecal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Tue", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Wed').checked){
						console.log("Send to user Wed[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == false && val.Wedcal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Wed", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Thu').checked){
						console.log("Send to user Thu[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == false && val.Thucal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Thu", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Fri').checked){
						console.log("Send to user Fri[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == false && val.Frical[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Fri", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
					if(document.getElementById('Sat').checked){
						console.log("Send to user Sat[]")
						for(let j = 0; j < lis.length; j++){
							if(lis[j] == false && val.Satcal[j] == true){
								out.push(true)
							}
							else{
								out.push(false)
							}
						}
						var data = {
							date: "Sat", 
							arr: out, 
							id: Number(localStorage.getItem('userId'))
						}
						http.put("/",data)
						out = []
					}
				}
				window.location.reload();
			}
			
		}
		
	}
	{/* Key variable is val */}
	return (
		<div>
		  <meta charSet="utf-8" />
		  <meta name="viewport" content="width=device-width, initial-scale=1" />
		  <title>Edit Profile</title>
		  <link rel="stylesheet" href="profile.css" />
		  <link rel="stylesheet" href="style.css" />
		  <link rel="stylesheet" href="EditP.css" />
		  <section className="container">
			<Nav />
		  </section>
		  {(() => {if(val == null) {
			return (
				<div className="centerText">Please Sign in</div>
			)
		  }else{
		    return(
			  <div className="EditPContainer">
				<div className="accountEdit">
				  <div className="accountLeftSection">
					<h2 className="EditTitle">
					  Hello
					</h2>
				  </div>
				  <div className="accountRightSection">
				  </div>
				</div>
				<div className="accountEdit">
				  <div className="accountLeftSection">
					<h2 className="EditTitle">
					  Picture
					</h2>
				  </div>
				  <div className="accountRightSection leftPad">
					<h4 className="littleHead">
					Current image
					</h4>
					<img className="imgItem" width={175} height={175} src={val.img} />
					<button className="editBtn loginBtn">
					  Upload picture
					</button>
				  </div>
				</div>
				<div className="accountEdit">
					<div className="accountLeftSection">
						<h2 className="EditTitle">
						Edit calendar times
						</h2>
					</div>
					<div className="accountRightSection">
						<h4 className="littleHead leftPad">
							Set the times that you are available. 
						</h4>
						<form className="Ree">
							<div className="checkboxes">
								<input type="radio" value="set" name="y/n" id="r1" checked/>
							<label>Add Times</label>
							</div>
							<div className="checkboxes">
								<input type="radio" value="remove" name="y/n" id="r2"/>
							<label>Remove Times</label>
							</div>
						</form>
						<div className="inputsHolder">
							<div className="checkboxHolder">
								<div className="checkboxes">
									<input type="checkbox" id="Sun"/>
										<label>
											Sunday
										</label>
								</div>
								<div className="checkboxes">
									<input type="checkbox" id="Mon"/>
										<label>
											Monday
										</label>
								</div>
								<div className="checkboxes" >
									<input type="checkbox" id="Tue"/>
										<label>
											Tuesday
										</label>
								</div>
								<div className="checkboxes" >
									<input type="checkbox" id="Wed"/>
										<label>
											Wednesday
										</label>
								</div>
							</div>
							<div className="checkboxHolder threeLeft">
								<div className="checkboxes">
									<input type="checkbox" id="Thu"/>
										<label>
											Thursday
										</label>
								</div>
								<div className="checkboxes">
									<input type="checkbox" id="Fri"/>
										<label>
											Friday
										</label>
								</div>
								<div className="checkboxes">
									<input type="checkbox" id="Sat"/>
										<label>
											Saturday
										</label>
								</div>
							</div>
							<input className="timeInputBox" placeholder="Start of availability" type="text" list="Starttimes" id="Stimes"/>
								<datalist id="Starttimes" name="start">
									<Times />
								</datalist>
							<input className="timeInputBox" placeholder="End of availability" type="text" list="Endtimes" id="Etimes" />
								<datalist id="Endtimes" name="end">
									<Times />
								</datalist>
						</div>
						<button className="editBtn loginBtn" onClick={updateCal}>
							Submit
						</button>
						<div className="marginBot">
							If timeframe doesn't exist (18:00 -> 9:00) nothing will happen.
							<br/>
							Please pick one of the options in the list. Otherwise nothing will happen.
							{/* <br/>
							Current Calendar
							<div className="calendarHolder">
								<Cal />
							</div> */}
						</div>
					</div>
				</div>
			  </div>
			)
		 }
		})()}
	  </div>
	)
	}
}

export default Editprofile;