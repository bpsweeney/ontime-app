import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/profile.css';
import '../CSS/style.css';
import '../CSS/group.css';
import groupData from '../Json/groupData.json';
import Nav from './components/userNav';
import http from '../../http-common'

class group extends Component {
	constructor(props){
		super(props);
		this.state = {
			curr_user: localStorage.getItem('userId'),
			groups: null
		}
	}
	componentDidMount() {
		http.get("/Groups")
		.then(res => res.data)
		.then(user => {
			this.setState({
				groups: user.users
			});
		});
	}
	render() {
	if(this.state.groups != null){
		var groupData = this.state.groups
		console.log(groupData)
	}
	return(
		<div>
			<section className="container">
			<Nav />
		    </section>
			{/* Did something almost identical to
			    fr.js and usersearch.js.    
				Hard code user as 1 again.      */}
			{(() => {if(groupData == null) {
				return (
					<div className="centerText">help</div>
				)
			} else{
				return (
					<div className="container">
					<div className="titles">Groups</div>
					<div className="GroupBox">
						{groupData.filter((val)=>{
						if(val.users.includes(this.state.curr_user)) {
							return val
						}
						else{
							return null
						}
						})
						.map((val, key)=> {
							return (
								<div className="GroupContents">
									<Link to={'/group/'+ val.name}>
									<a href className="GroupLink">
										{val.name}
									</a>
									</Link>
								</div>
						);
						})
						}
					</div>
					<Link to='/Makegroup'>
					<button className="makeGroupBtn"> + Create Group </button>
					</Link>
					</div>
				)
			}
			})()}
		</div>
	);
	}
}

export default group;