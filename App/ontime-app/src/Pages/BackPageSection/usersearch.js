import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import data from "../Json/friendTest.json";
import '../CSS/profile.css';
import '../CSS/style.css';
import '../CSS/search.css';
import Nav from './components/userNav';

class usersearch extends Component {
	constructor(props){
		super(props);
		this.state = {
			searchTerm: ""
		};
	}
	render() {
	return(
		<div>
		{/*Skip past this */}
		  <section className="container">
			<Nav />
		  </section>
		  {/*Start new stuff*/}
			<div className="container">
			<div className="textbox">
			<div className="titles">
				User Search
			</div>
			{/*<p>
				Type the name of a user.
			</p>*/}
			{/* Removing state from the search term (set and check) made this work properly. */}
			<input className="Searchbox" type="text" placeholder="Search" onChange={e => this.setState({searchTerm: this.searchTerm = e.target.value})}/>
			{data.filter((val)=>{
				if(this.searchTerm == "") {
					return null
				}
				else if(val.name.includes(this.searchTerm)){
					return val
				}
			})
			.map((val, key)=> {
				return (
				<div className="results" key={key}>
				<Link to={'/user/'+ val.id}>
				<a href>
				<div className="FixLinks">
					{val.name}
				</div>
				</a>
				</Link>
				</div>
				);
			})}
			</div>
			</div>
		</div>
	);
	}
}

export default usersearch;