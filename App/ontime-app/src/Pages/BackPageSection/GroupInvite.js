import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/style.css';
import '../CSS/profile.css';
import '../CSS/EditP.css';
import Nav from './components/userNav';
import http from '../../http-common';
//import groupData from '../Json/groupData.json';
//
// I don't know if the functions should be in the render or not.
// It seems like they shouldn't be.
// Please tell me which is better for the backend.
// curr_invites will be the only issue trying to move the function
// outside of render right now. To fix this I would make this.state
// have another field of curr_invites and append to it the return.
// Then to call the functions I would remove function and then add
// this. to the calls to them in the return.
//
// This might be important so I thought it would be worth pointing out.
//
// I also don't know if this scheme would work, but it is what I would try to do first.
//

class Invites extends Component {
	constructor(props){
		super(props);
		this.state = {
			curr_user: localStorage.getItem('userId'),
			groups: null
		};
	}
	componentDidMount() {
		http.get("/Groups")
		.then(res => res.data)
		.then(user => {
			this.setState({
				groups: user.users
			});
		});
	}
	render() {
	function filterByname(data, name){
		return data.filter(function(data){return (data.name == name);})[0];
	}
	if(this.state.groups != null){
		var groupData = this.state.groups
		console.log(groupData)
	}
	{/* Key variable is val */}
	{/*var val = filterById(data, this.state.curr_user)*/}
	var curr_invites = [];
	function Join(e){
		e.preventDefault();
		var firedButton = e.target.id;
		var ele = document.getElementById(curr_invites[firedButton].name);
		ele.remove();
		// Change the json in groups here?
		const groupName = curr_invites[firedButton].name;
		var Musers = curr_invites[firedButton].users;
		Musers.push(localStorage.getItem('userId'));
		var oldInvites = curr_invites[firedButton].PendingInvites;
		var newInvites = [];
		for(let k = 0; k < oldInvites.length; k++){
				//Note !== will check datatype. So we don't want that
				//This is a int vs string compare
			if(oldInvites[k] != localStorage.getItem('userId')){
				newInvites.push(oldInvites[k]);
			}
		}
		const indata = {
			nUsers: Musers, 
			nInvite: newInvites, 
			group: groupName,
		}
		http.put("/accInvite",indata)
		alert("You Joined the group " + curr_invites[firedButton].name)
	}
	function Remove(e){
		e.preventDefault();
		var firedButton = e.target.id;
		var ele = document.getElementById(curr_invites[firedButton].name);
		ele.remove();
		//Change the json in groups here?
		const groupName = curr_invites[firedButton].name;
		var oldInvites = curr_invites[firedButton].PendingInvites;
		var newInvites = [];
		for(let k = 0; k < oldInvites.length; k++){
				//Note !== will check datatype. So we don't want that
				//This is a int vs string compare
			if(oldInvites[k] != localStorage.getItem('userId')){
				newInvites.push(oldInvites[k]);
			}
		}
		const outdata = {
			nInvites: newInvites,
			group: groupName,
		}
		http.put("/denyInvite",outdata)
		alert("You declined the invite to " + curr_invites[firedButton].name)
	}
	return (
		<div>
		  <meta charSet="utf-8" />
		  <meta name="viewport" content="width=device-width, initial-scale=1" />
		  <link rel="stylesheet" href="profile.css" />
		  <link rel="stylesheet" href="style.css" />
		  <link rel="stylesheet" href="EditP.css" />
		  <section className="container">
			<Nav />
		  </section>
		  <div className="container">
		  {/* Wanted to use the friend stuff again */}
		  <div className="friendBox">
				<h2 className="semititle">Current Invites</h2>
		{(() => {if(groupData != null){
			return(
			<div className="useritems">
			  {groupData.filter((val)=>{
					if(val.PendingInvites.includes(Number(this.state.curr_user))) {
						curr_invites.push(val)
						console.log(val)
						return val
					}
					else{
						console.log(val)
						return null
					}
					})
					.map((val, key)=> {
						return (
							<div className="userBox forinvite" id = {val.name}>
								<Link to={'/group/'+ val.name}>
								<a href className="GroupLink invite">
									{val.name}
								</a>
								</Link>
								<div className="BtnsHolder">
									<button className="acceptbtn" id = {key} onClick={Join}>
										Join
									</button>
									<button className="declinebtn" id = {key} onClick={Remove}>
										Decline
									</button>
								</div>
							</div>
						);
					})
			  }
		  </div>
		  )
		  }
		  })()}
		  </div>
		  </div>
		</div>
		
	)
	}
}

export default Invites;