import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/style.css';
import '../CSS/profile.css';
//Old json file usage.
//import data from '../Json/friendTest.json';
import Nav from './components/userNav';
import http from '../../http-common';


// If you can, I would try to import all users as data.
// As long as the fields match my test json this should work.
// The ids won't be numbers so there will be some minor changes that I think you can figure out.


class users extends Component { 
	constructor(props){
		super(props);
		this.state = {
			curr_user: localStorage.getItem('userId'),
			userId: this.props.match.params.userId,
			users: null
		};
	}
	componentDidMount() {
		http.get()
		.then(res => res.data)
		.then(user => {
			this.setState({
				users: user.users
			});
		});
	}
	render() {
	function filterById(data, id){
		return data.filter(function(data){return ( data.id == id);})[0];
	}
	//console.log(data)
	{/* Key variable is val */}
	if(this.state.users != null){
		var val = filterById(this.state.users, this.state.userId)
	}
	//var val = filterById(data, this.state.userId)
	var calendar = [<div className="Days firstU">Sun</div>,<div className="Days">Mon</div>,<div className="Days">Tue</div>,<div className="Days">Wed</div>,<div className="Days">Thu</div>,<div className="Days">Fri</div>,<div className="Days">Sat</div>]
	var lis = [];
	var returnVals = [];
	var i = 100;
	var apptext= "AM";
	for(var j = 0; i < 2501; j++){
		var m = 0;
		if(i > 1245){
			apptext = "PM";
			m = i - 1200;
		}
		else{
			m = i;
		}
		if(i >= 2500){
			apptext= "1:00AM";
			m = 0;
		}
		var text = m.toString();
		text = text.replace(/(.{2})$/,':$1');
		text = text + apptext;
		lis.push(text);
		if(j == 3){
			i = i + 40;
			j = -1;
		}
		i = i + 15;
	}
	if(val != null){
		var vals = [];
		returnVals = [<div className="removeBG fixtext">{lis[0]}</div>];
		for(let a = 0; a < val.Moncal.length; a++){
			var attempt = a + 1;
			vals.push(<div className="removeBG fixtext">{lis[attempt]}</div>)
			if(val.Suncal[a] == true){
				if(lis[attempt].length == 7){
					vals.push(<div className="GreenBox"></div>);
				}
				else{
					vals.push(<div className="GreenBox Leff"></div>);
				}
			}else{
				if(lis[attempt].length == 7){
					vals.push(<div className="GrayBox"></div>);
				}
				else{
					vals.push(<div className="GrayBox Leff"></div>);
				}
			}
			if(val.Moncal[a] == true){
				vals.push(<div className="GreenBox"></div>);
			}else{
				vals.push(<div className="GrayBox"></div>);
			}
			if(val.Tuecal[a] == true){
				vals.push(<div className="GreenBox"></div>);
			}else{
				vals.push(<div className="GrayBox"></div>);
			}
			if(val.Wedcal[a] == true){
				vals.push(<div className="GreenBox"></div>);
			}else{
				vals.push(<div className="GrayBox"></div>);
			}
			if(val.Thucal[a] == true){
				vals.push(<div className="GreenBox"></div>);
			}else{
				vals.push(<div className="GrayBox"></div>);
			}
			if(val.Frical[a] == true){
				vals.push(<div className="GreenBox"></div>);
			}else{
				vals.push(<div className="GrayBox"></div>);
			}
			if(val.Satcal[a] == true){
				vals.push(<div className="GreenBox"></div>);
			}else{
				vals.push(<div className="GrayBox"></div>);
			}
			returnVals.push(<div className="rowDisplay">{vals}</div>);
			vals = [];
		}
	}
	
	return (
		<div>
		  <meta charSet="utf-8" />
		  <meta name="viewport" content="width=device-width, initial-scale=1" />
		  <title>Profile</title>
		  <link rel="stylesheet" href="profile.css" />
		  <link rel="stylesheet" href="style.css" />
		  <section className="container">
			<Nav />
		  </section>
		  {/*Learned how to make an if
		  {(() => {if(Number(this.state.userId) == 3) {
			return (
				<div>You are a Admin.</div>
			)
		  } 
		  })()}
		  */}
		  {(() => {if(val == null) {
			return (
				<div className="centerText">User does not exist.</div>
			)
		  } else{
			return(
				<div className="profileContainer">
				<div className="titles">{val.name}'s Profile </div>
				<div className="profileBox">
				<div className="profileHeader">
				<div className="ornament" style={{borderBottomColor: val.color,borderLeftColor: val.color }}/>
				{/* Hold the image somewhere in the backend?
						Also need a default image probably.*/}
				<img className="imgItem" width={175} height={175} src={val.img} />
				<div className="name-links">
				  <div className="semititle">
					UserName?
				  </div>
				  <div className="userLinks">
					<span>Links: </span> 
					<a href>
					  A Link
					</a>
					<span>, </span>
					<a href>
					  Another Link
					</a>
				  </div>
				</div>
			  </div>
			  <div className="profilebody">
				{(() => {if(this.state.curr_user == val.id){
					return(
						<div className="linkBtns">
							<Link to='/Editprofile'>
								<a href ><button className="pageBtns firstLinkm">Edit Profile</button></a>
							</Link>
						</div>
					)
				}else if(this.state.curr_user == null){
					return null
				}else{
					return(
						<div className="linkBtns">
						{/* {(() => {if(val.friends.includes(Number(this.state.curr_user))){
							return(
								<Link to='/Editprofile'>
									<a href ><button className="pageBtns firstLinko">Unfriend</button></a>
								</Link>
							)
						}else{
							return(
								<Link to='/Editprofile'>
									<a href ><button className="pageBtns firstLinko">Friend</button></a>
								</Link>
							)
						}
						})()} */}
						<Link to='/groups'>
							<a href ><button className="pageBtns firstLinko">Invite to a group</button></a>
						</Link>
						</div>
					)
				}
				})()}
				{/* Start user calendat section */}
				<div className="calendarHolder">
					{val.name}'s current calendar.
					<div className="rowDisplay">
						<div className="GreenBox keyBox" /> = Time frame is currently set
						<div className="GrayBox keyBox margLeft" /> = Time frame is not set
					</div>
				<div className="rowDisplay">
					{calendar}
				</div>
					{returnVals}
				</div>
			  </div>
			</div>
		  </div>
			)
		  }
		  })()}
		</div>
	)
	}
}

export default users;




