import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/profile.css';
import '../CSS/style.css';
import '../CSS/group.css';
//import groupData from '../Json/groupData.json';
//import data from '../Json/friendTest.json';
import http from '../../http-common';
import Nav from './components/userNav';
import Times from './components/TimesDropdown.js';

class groupItem extends Component {
	constructor(props){
		super(props);
		this.state = {
			curr_user: localStorage.getItem('userId'),
			groupName: this.props.match.params.groupName,
			groups: null,
			users: null
		};
	}
	componentDidMount() {
		http.get("")
		.then(res => res.data)
		.then(user => {
			this.setState({
				users: user.users
			});
		});
		http.get("/Groups")
		.then(res => res.data)
		.then(user => {
			this.setState({
				groups: user.users
			});
		});
	}
	render() {
	if(this.state.groups != null){
		var groupData = this.state.groups
		var val = filterByName(groupData, this.state.groupName);
		//console.log(groupData)
		//console.log(val)
	}
	if(this.state.users != null){
		var udata = this.state.users
		var Userval = filterById(udata, this.state.curr_user);
		//console.log(data)
		//console.log(Userval)
	}
	function filterByName(groupData, name){
		return groupData.filter(function(groupData){return (groupData.name == name);})[0];
	}
	function filterById(data, id){
		return data.filter(function(data){return ( data.id == id);})[0];
	}
	function EditGroup(e){
		e.preventDefault();
		const Group = document.getElementById("GroupName");
		const about = Group.Aboutus.value;
		const invite = Group.Invites.value;
		let StartVal = document.getElementById("Stimes").value;
		let EndVal = document.getElementById("Etimes").value;
		let SVal = parseFloat(StartVal.replace(/:/g, ''));
		let EVal = parseFloat(EndVal.replace(/:/g, ''));
		//console.log(about);
		//console.log(invite);
		//console.log(StartVal);
		//console.log(EVal);
		if(about == "" && invite == "" && (StartVal == "" || EndVal == "")){
			console.log("No data to set.")
		}
		else{
			if(StartVal == "" || EndVal == ""){
				console.log("Don't set times");
			}
			else{
				if(EVal <= SVal){
					console.log("Invalid time frame")
				}
				else{
					let lis = [];
					let i = 100;
					let state = false;
					for(let j = 0; i < 2501; j++){
						if(i >= SVal && i <= EVal){
							if(state == false){
								state = true;
							}
							else
								lis.push(true);
								state = true
						}
						else{
							lis.push(false);
							state = false;
						}
						if(j == 3){
							i = i + 40
							j = -1
						}
						i = i + 15
					}
					if(lis.length == 97){
						alert("Not valid input.")
					}
					else{
						//console.log(lis);
						if(document.getElementById('r1').checked) {
							console.log("Do a set meet times")
						// TT = T, TF = T, FT = T, FF = F
							var out = [];
							if(document.getElementById('Sun').checked){
								console.log("Send to meet Sun[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == true || val.SunMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Sun", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								//We are going to check id against the owner value of the group.
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Mon').checked){
								console.log("Send to meet Mon[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == true || val.MonMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Mon", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Tue').checked){
								console.log("Send to meet Tue[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == true || val.TueMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Tue", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Wed').checked){
								console.log("Send to meet Wed[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == true || val.WedMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Wed", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Thu').checked){
								console.log("Send to meet Thu[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == true || val.ThuMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Thu", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Fri').checked){
								console.log("Send to meet Fri[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == true || val.FriMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Fri", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Sat').checked){
								console.log("Send to meet Sat[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == true || val.SatMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Sat", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
						}
						else{
							console.log("Do a remove")
							var out = [];
							// First array is lis, second one is the current user array.
							// FF = F, FT = T, TF = F, TT = F
							if(document.getElementById('Sun').checked){
								console.log("Send to meet Sun[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == false && val.SunMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Sun", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Mon').checked){
								console.log("Send to meet Mon[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == false && val.MonMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Mon", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Tue').checked){
								console.log("Send to meet Tue[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == false && val.TueMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Tue", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Wed').checked){
								console.log("Send to meet Wed[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == false && val.WedMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Wed", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Thu').checked){
								console.log("Send to meet Thu[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == false && val.ThuMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Thu", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Fri').checked){
								console.log("Send to meet Fri[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == false && val.FriMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Fri", 
									arr: out, 
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
							if(document.getElementById('Sat').checked){
								console.log("Send to meet Sat[]")
								for(let j = 0; j < lis.length; j++){
									if(lis[j] == false && val.SatMeetTimes[j] == true){
										out.push(true)
									}
									else{
										out.push(false)
									}
								}
								var data = {
									date: "Sat", 
									arr: out,
									group: val.name,
									id: Number(localStorage.getItem('userId'))
								}
								http.put("/GCal",data)
								out = []
							}
						}
					}
				}
			}
			if(about != ""){
				var data = {
					newAbout: about,
					group: val.name,
					id: localStorage.getItem('userId')
				}
				http.put("/GroupAbout",data)
			}
			if(invite != ""){
				var invites = [];
				for(let m = 0; m < val.PendingInvites.length; m++){
					invites.push(val.PendingInvites[m]);
				}
				var musers = [];
				musers = invite.split(" ");
				var returneduser = 0;
				for(let m = 0; m < musers.length; m++){
					returneduser = filterByName(udata, musers[m])
					if(returneduser != undefined && !invites.includes(returneduser.id)){
						//Can't invite users already in the group
						if(!val.users.includes(returneduser.id.toString())){
							invites.push(returneduser.id)
						}
					}
				}
				if(invites.length > val.PendingInvites.length){
					//Time to set the new PendingInvites array.
					var data = {
						newInvites: invites,
						group: val.name,
						id: localStorage.getItem('userId')
					}
					http.put("/addInvites",data)
				}
			}
			window.location.reload();
		}
	}
	{/* Key variable is val */}
	//var val = filterByName(groupData, this.state.groupName);
	var calendar = [<div className="Days firstU">Sun</div>,<div className="Days">Mon</div>,<div className="Days">Tue</div>,<div className="Days">Wed</div>,<div className="Days">Thu</div>,<div className="Days">Fri</div>,<div className="Days">Sat</div>];
	//var Userval = filterByName(data, this.state.userId);
	var lis = [];
	var returnVals = [];
	var i = 100;
	var apptext= "AM";
	if(val != null){
		if(val.users.includes((this.state.curr_user))){
			for(var j = 0; i < 2501; j++){
				var m = 0;
				if(i > 1245){
					apptext = "PM";
					m = i - 1200;
				}
				else{
					m = i;
				}
				if(i >= 2500){
					apptext= "1:00AM";
					m = 0;
				}
				var text = m.toString();
				text = text.replace(/(.{2})$/,':$1');
				text = text + apptext;
				lis.push(text);
				if(j == 3){
					i = i + 40;
					j = -1;
				}
				i = i + 15;
			}
			
			if(val != null){
				var vals = [];
				returnVals = [<div className="removeBG fixtext">{lis[0]}</div>];
				for(let a = 0; a < val.MonMeetTimes.length; a++){
					var attempt = a + 1;
					vals.push(<div className="removeBG fixtext">{lis[attempt]}</div>)
					if(val.SunMeetTimes[a] == true){
						if(lis[attempt].length == 7){
							vals.push(<div className="GreenBox"></div>);
						}
						else{
							vals.push(<div className="GreenBox Leff"></div>);
						}
					}else{
						if(lis[attempt].length == 7){
							vals.push(<div className="GrayBox"></div>);
						}
						else{
							vals.push(<div className="GrayBox Leff"></div>);
						}
					}
					if(val.MonMeetTimes[a] == true){
						vals.push(<div className="GreenBox"></div>);
					}else{
						vals.push(<div className="GrayBox"></div>);
					}
					if(val.TueMeetTimes[a] == true){
						vals.push(<div className="GreenBox"></div>);
					}else{
						vals.push(<div className="GrayBox"></div>);
					}
					if(val.WedMeetTimes[a] == true){
						vals.push(<div className="GreenBox"></div>);
					}else{
						vals.push(<div className="GrayBox"></div>);
					}
					if(val.ThuMeetTimes[a] == true){
						vals.push(<div className="GreenBox"></div>);
					}else{
						vals.push(<div className="GrayBox"></div>);
					}
					if(val.FriMeetTimes[a] == true){
						vals.push(<div className="GreenBox"></div>);
					}else{
						vals.push(<div className="GrayBox"></div>);
					}
					if(val.SatMeetTimes[a] == true){
						vals.push(<div className="GreenBox"></div>);
					}else{
						vals.push(<div className="GrayBox"></div>);
					}
					returnVals.push(<div className="rowDisplay">{vals}</div>);
					vals = [];
				}
				console.log(returnVals)
			}
		}
	}
	return(
		<div>
			<section className="container">
			   <Nav />
		    </section>
			<div className="container">
			{(() => {if(val != null && this.state.curr_user == val.owner) {
				return (
					<div className="sectionHolder">
						<div className="titles">
							Set up meeting times?
						</div>
						<div className="firstBox textbox">
							<form id="GroupName" className="nameInput">
								<div className="semititle textFix">Update what your group is about.</div>
								<input type="text" className="inputField" name="Aboutus" placeholder="Change the about us" />
								{/*Feels like the simplest way to do it.*/}
								<div className="semititle textFix">Invite users</div>
								<input type="text" className="inputField" name="Invites" placeholder="Space separated list of new users to invite" />
								<h4 className="littleHead leftPad">
									Set the times to meet. 
								</h4>
							</form>	
						<form className="Ree">
							<div className="checkboxes">
								<input type="radio" value="set" name="y/n" id="r1" checked/>
							<label>Add Times</label>
							</div>
							<div className="checkboxes">
								<input type="radio" value="remove" name="y/n" id="r2"/>
							<label>Remove Times</label>
							</div>
						</form>
						<div className="inputsHolder">
							<div className="checkboxHolder">
								<div className="checkboxes">
									<input type="checkbox" id="Sun"/>
										<label>
											Sunday
										</label>
								</div>
								<div className="checkboxes">
									<input type="checkbox" id="Mon"/>
										<label>
											Monday
										</label>
								</div>
								<div className="checkboxes" >
									<input type="checkbox" id="Tue"/>
										<label>
											Tuesday
										</label>
								</div>
								<div className="checkboxes" >
									<input type="checkbox" id="Wed"/>
										<label>
											Wednesday
										</label>
								</div>
							</div>
							<div className="checkboxHolder threeLeft">
								<div className="checkboxes">
									<input type="checkbox" id="Thu"/>
										<label>
											Thursday
										</label>
								</div>
								<div className="checkboxes">
									<input type="checkbox" id="Fri"/>
										<label>
											Friday
										</label>
								</div>
								<div className="checkboxes">
									<input type="checkbox" id="Sat"/>
										<label>
											Saturday
										</label>
								</div>
							</div>
							<input className="timeInputBox" placeholder="Start of meeting" type="text" list="Starttimes" id="Stimes"/>
								<datalist id="Starttimes" name="start">
									<Times />
								</datalist>
							<input className="timeInputBox" placeholder="End of meeting" type="text" list="Endtimes" id="Etimes" />
								<datalist id="Endtimes" name="end">
									<Times />
								</datalist>
						</div>
						</div>
						<div className="secondBox textbox">
							<div className="Extra">
								<button type="submit" className="makeGroupBtn" onClick={EditGroup}>
									Submit Changes
								</button>
							</div>
						</div>
					</div>
				)
			} 
			})()}
			<div className="sectionHolder">
			<div className="titles">
				Current meeting times for this group.
			</div>
			{(() => {if(val != null && val.users.includes((this.state.curr_user))){
				return(
					<div className="firstBox textbox">
						<h1 className="RemoveBG">{val.name} meeting times.</h1>
						<div className="rowDisplay">
							<div className="GreenBox keyBox" /> = Time frame is currently set
							<div className="GrayBox keyBox margLeft" /> = Time frame is not set
						</div>
						<div className="rowDisplay">
							{calendar}
						</div>
						{returnVals}
					</div>
				)
				}else{
					return(
						<div className="firstBox textbox">
							You must be a member of this group to see meeting times.
						</div>
					)
				}
			})()}
			{(() => {if(val != null) {
				return (
					<div className="secondBox textbox">
						<div className="semititle">
							What is this group?
						</div>
						{val.aboutUs}
					</div>
				)
			} 
			})()}
			</div>
			<div className="titles">
				Members in this group?
			</div>
			{(() => {if(udata != null && val != null) {
				return (
				<div className="secondBox textbox fix-top-radius">
					{udata.filter((val2)=>{
					if(val.users.includes(String(val2.id))) {
						return val2
					}
					else{
						return null
					}
					})
					.map((val2, key)=> {
						return (
							<div className="Euser-helper">
								<Link to={'/user/'+ val2.id}>
								<a href className="Eusers GroupLink">
									{val2.name}
								</a>
								</Link>
							</div>
					);
					})
					}
				</div>
				)
				} 
			})()}
			</div>
		</div>
	);
	}
}

export default groupItem;