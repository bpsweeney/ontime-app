import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from './Pages/StartPageSection/Home.js';
import SignIn from './Pages/StartPageSection/SignIn.js';
import SignUp from './Pages/StartPageSection/SignUp.js';
import Editprofile from './Pages/BackPageSection/Editprofile.js';
import usersearch from './Pages/BackPageSection/usersearch.js';
import fr from './Pages/BackPageSection/fr.js';
import oUsers from './Pages/BackPageSection/users.js';
import group from './Pages/BackPageSection/group.js';
import groupL from './Pages/BackPageSection/groupListing.js';
import makeGroup from './Pages/BackPageSection/createGroup.js';
import invites from './Pages/BackPageSection/GroupInvite.js';

function App() {
	return (
		<div className='App'>
		<Router>
		<Switch>
			<Route exact path='/' component={Home}/>
			<Route exact path='/SignIn' component={SignIn}/>
			<Route exact path='/SignUp' component={SignUp}/>
			<Route exact path='/Editprofile' component={Editprofile}/>
			<Route path='/user/:userId' component={oUsers}/>
			<Route exact path='/usersearch' component={usersearch}/>
			<Route exact path='/friends' component={fr}/>
			<Route exact path='/groups' component={group}/>
			<Route path='/group/:groupName' component={groupL}/>
			<Route path='/Makegroup' component={makeGroup}/>
			<Route path='/Invites' component={invites}/>
		</Switch>
		</Router>
		</div>
	)
}

export default App;
