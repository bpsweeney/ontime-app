import mongodb from "mongodb"
const ObjectId = mongodb.ObjectID

let groups

export default class GroupDAO {
	static async injectDB(conn) {
		if (groups) {
		  return
		}
		try {
			groups = await conn.db(process.env.USERS_NS).collection("Organizations")
		} catch (e) {
		  console.error(
			`Unable to establish a collection handle in groupsDAO: ` +e,
		  )
		}
	}
	static async InsertGroup(value){
		try{
			//console.log(value)
			return await groups.insertOne(value)
		}catch (e){
			console.error(`Can not make group: ` +e,)
			return {error: e}
		}
	}
	static async getGroups(){
 		let cursor
		try{
			cursor = await groups.find()
		}catch (e){
			console.error(`Can find groups: ` +e,)
		}
		try{
			const groupList = await cursor.toArray()
			return groupList
		}catch(e){
			console.error(`Unable to convert cursor to array, ${e}`,)
			return {error: e}
		} 
	}
	static async changeAbout(about, userId, currGroup){
		try{
			const updateAbout = await groups.updateOne(
				{owner: userId, name: currGroup},
				{ $set: {aboutUs: about}},
			)
			return updateAbout
		}catch(e){
			console.error(`Can not update cal: ` +e,)
			return {error: e}
		}
	}
	static async changeInvites(invitee, userId, currGroup){
		try{
			const updateI = await groups.updateOne(
				{owner: userId, name: currGroup},
				{ $set: {PendingInvites: invitee}},
			)
			return updateI
		}catch(e){
			console.error(`Can not update cal: ` +e,)
			return {error: e}
		}
	}
	static async accInvite(invitee, neusers, currGroup){
		try{
			const update = await groups.updateOne(
				{name: currGroup},
				{ $set: {PendingInvites: invitee, users: neusers}},
			)
			return update
		}catch(e){
			console.error(`Can not update cal: ` +e,)
			return {error: e}
		}
	}
	static async denyInvite(invitee, currGroup){
		try{
			const update = await groups.updateOne(
				{name: currGroup},
				{ $set: {PendingInvites: invitee}},
			)
			return update
		}catch(e){
			console.error(`Can not update cal: ` +e,)
			return {error: e}
		}
	}
	static async changeCal(userId, arr, date, currGroup){
		try{
			var updateCal = null
			if(date == "Sun"){
				updateCal = await groups.updateOne(
				{owner: userId, name: currGroup},
				{ $set: { SunMeetTimes: arr}},
			)}
			else if(date == "Mon"){
				updateCal = await groups.updateOne(
				{owner: userId, name: currGroup},
				{ $set: { MonMeetTimes: arr}},
			)}
			else if(date == "Tue"){
				updateCal = await groups.updateOne(
				{owner: userId, name: currGroup},
				{ $set: { TueMeetTimes: arr}},
			)}
			else if(date == "Wed"){
				updateCal = await groups.updateOne(
				{owner: userId, name: currGroup},
				{ $set: { WedMeetTimes: arr}},
			)}
			else if(date == "Thu"){
				updateCal = await groups.updateOne(
				{owner: userId, name: currGroup},
				{ $set: { ThuMeetTimes: arr}},
			)}
			else if(date == "Fri"){
				updateCal = await groups.updateOne(
				{owner: userId, name: currGroup},
				{ $set: { FriMeetTimes: arr}},
			)}
			else if(date == "Sat"){
				updateCal = await groups.updateOne(
				{owner: userId, name: currGroup},
				{ $set: { SatMeetTimes: arr}},
			)}
			return updateCal
		}catch(e){
			console.error(`Can not update cal: ` +e,)
			return {error: e}
		}
	}
}