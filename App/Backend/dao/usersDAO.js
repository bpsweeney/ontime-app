import mongodb from "mongodb"
const ObjectId = mongodb.ObjectID
let users

export default class UsersDAO {
  static async injectDB(conn) {
    if (users) {
      return
    }
    try {
        users = await conn.db(process.env.USERS_NS).collection("Users")
    } catch (e) {
      console.error(
        `Unable to establish a collection handle in usersDAO: ` +e,
      )
    }
  }

	static async InsertUser(value){
		try{
			//console.log(value)
			return await users.insertOne(value)
		}catch (e){
			console.error(`Can not make user: ` +e,)
			return {error: e}
		}
	}
	
	static async changeCal(userId, arr, date){
		try{
			var updateCal = null
			if(date == "Sun"){
				updateCal = await users.updateOne(
				{id: userId},
				{ $set: { Suncal: arr}},
			)}
			else if(date == "Mon"){
				updateCal = await users.updateOne(
				{id: userId},
				{ $set: { Moncal: arr}},
			)}
			else if(date == "Tue"){
				updateCal = await users.updateOne(
				{id: userId},
				{ $set: { Tuecal: arr}},
			)}
			else if(date == "Wed"){
				updateCal = await users.updateOne(
				{id: userId},
				{ $set: { Wedcal: arr}},
			)}
			else if(date == "Thu"){
				updateCal = await users.updateOne(
				{id: userId},
				{ $set: { Thucal: arr}},
			)}
			else if(date == "Fri"){
				updateCal = await users.updateOne(
				{id: userId},
				{ $set: { Frical: arr}},
			)}
			else if(date == "Sat"){
				updateCal = await users.updateOne(
				{id: userId},
				{ $set: { Satcal: arr}},
			)}
			return updateCal
		}catch(e){
			console.error(`Can not update cal: ` +e,)
			return {error: e}
		}
	}
  static async getUsers({
    filters = null,
    page = 0,
    usersPerPage = 20,
  } = {}) {
    let query
    if (filters) {
      if ("name" in filters) {
        query = { $text: { $search: filters["name"] } }
      } 
      else if ("email" in filters) {
        query = { "email": { $eq: filters["email"] } }
      }
    }

    let cursor
    
    try {
      cursor = await users
        .find(query)
    } catch (e) {
      console.error(`Unable to issue find command, ${e}`)
      return { usersList: [], totalNumUsers: 0 }
    }

    const displayCursor = cursor.limit(usersPerPage).skip(usersPerPage * page)

    try {
      const usersList = await displayCursor.toArray()
      const totalNumUsers = await users.countDocuments(query)

      return { usersList, totalNumUsers }
    } catch (e) {
      console.error(
        `Unable to convert cursor to array or problem counting documents, ${e}`,
      )
      return { usersList: [], totalNumUsers: 0 }
    }
  }
//   static async getRestaurantByID(id) {
//     try {
//       const pipeline = [
//         {
//             $match: {
//                 _id: new ObjectId(id),
//             },
//         },
//               {
//                   $lookup: {
//                       from: "reviews",
//                       let: {
//                           id: "$_id",
//                       },
//                       pipeline: [
//                           {
//                               $match: {
//                                   $expr: {
//                                       $eq: ["$restaurant_id", "$$id"],
//                                   },
//                               },
//                           },
//                           {
//                               $sort: {
//                                   date: -1,
//                               },
//                           },
//                       ],
//                       as: "reviews",
//                   },
//               },
//               {
//                   $addFields: {
//                       reviews: "$reviews",
//                   },
//               },
//           ]
//       return await restaurants.aggregate(pipeline).next()
//     } catch (e) {
//       console.error(`Something went wrong in getRestaurantByID: ${e}`)
//       throw e
//     }
//   }

//   static async getCuisines() {
//     let cuisines = []
//     try {
//       cuisines = await restaurants.distinct("cuisine")
//       return cuisines
//     } catch (e) {
//       console.error(`Unable to get cuisines, ${e}`)
//       return cuisines
//     }
//   }
}



