import express from "express"
import cors from "cors"
import users from "./api/users.route.js"

const app = express()
// const express = require('express')

const port = 5000

// app.get('/', (req, res) => {
//   res.send('Hello World!')
// })

// app.listen(port, () => {
//   console.log(`Example app listening at http://localhost:${port}`)
// })

app.use(cors())
app.use(express.json())

app.use("/api/v1/users", users)
app.use("*", (req, res) => res.status(404).json({ error: "not found"}))

export default app