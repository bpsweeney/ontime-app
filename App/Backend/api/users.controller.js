import UsersDAO from "../dao/usersDAO.js"

export default class UsersController {
	static async apiPostUser(req, res, next){
		try{
			//console.log(req.body)
			return await UsersDAO.InsertUser(req.body)
		}catch (e){
			console.error(e)
			return {error: e}
		}
	}
	static async apiUpdateUser(req, res, next){
		try{
			const arr = req.body.arr
			const date = req.body.date
			const userId = req.body.id
			const request = await UsersDAO.changeCal(userId, arr, date)
			res.json("Updated")
		}catch (e){
			console.error(e)
			return {error: e}
		}
	}
    static async apiGetUsers(req, res, next) {
      const usersPerPage = req.query.usersPerPage ? parseInt(req.query.usersPerPage, 10) : 20
      const page = req.query.page ? parseInt(req.query.page, 10) : 0
  
      let filters = {}
      if (req.query.email) {
        filters.email = req.query.email
      } else if (req.query.name) {
        filters.name = req.query.name
      }
  
      const { usersList, totalNumUsers } = await UsersDAO.getUsers({
        filters,
        page,
        usersPerPage,
      })
  
      let response = {
        users: usersList,
        page: page,
        filters: filters,
        entries_per_page: usersPerPage,
        total_results: totalNumUsers,
      }
      res.json(response)
    }
    // static async apiGetUserById(req, res, next) {
    //   try {
    //     let id = req.params.id || {}
    //     let user = await UsersDAO.apiGetUserById(id)
    //     if (!user) {
    //       res.status(404).json({ error: "Not found" })
    //       return
    //     }
    //     res.json(user)
    //   } catch (e) {
    //     console.log(`api, ` +e)
    //     res.status(500).json({ error: e })
    //   }
    // }
  
    // static async apiGetUserEmail(req, res, next) {
    //   try {
    //     let email = await UsersDAO.getEmail()
    //     res.json(email)
    //   } catch (e) {
    //     console.log(`api, ` +e)
    //     res.status(500).json({ error: e })
    //   }
    // }
  }