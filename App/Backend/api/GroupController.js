import groupDAO from "../dao/groupDAO.js"

export default class GroupController {
	static async apiPostGroup(req, res, next){
		try{
			//console.log(req.body)
			return await groupDAO.InsertGroup(req.body)
		}catch (e){
			console.error(e)
			return {error: e}
		}
	}
	static async apiGetGroups(req, res, next){
		const val = await groupDAO.getGroups()
		let response = {
			users: val
		}
		res.json(response)
	}
	static async apiAccInvite(req, res, next){
		try{
			const neusers = req.body.nUsers
			const invitee = req.body.nInvite
			const currGroup = req.body.group
			const request = await groupDAO.accInvite(invitee, neusers, currGroup)
		}catch (e){
			console.error(e)
			return {error: e}
		}
	}
	static async apiDenyInvite(req, res, next){
		try{
			const invitee = req.body.nInvites
			const currGroup = req.body.group
			const request = await groupDAO.denyInvite(invitee, currGroup)
		}catch (e){
			console.error(e)
			return {error: e}
		}
	}
	static async apiAddInvite(req, res, next){
		try{
			const invitee = req.body.newInvites
			const currGroup = req.body.group
			const userId = req.body.id
			const request = await groupDAO.changeInvites(invitee, userId, currGroup)
		}catch (e){
			console.error(e)
			return {error: e}
		}
	}
	static async apiUpdateCal(req, res, next){
		try{
			const arr = req.body.arr
			const date = req.body.date
			var intId = req.body.id
			const userId = intId.toString()
			const currGroup = req.body.group
			const request = await groupDAO.changeCal(userId, arr, date, currGroup)
			res.json("Updated")
		}catch (e){
			console.error(e)
			return {error: e}
		}
	}
	static async apiUpdateAbout(req, res, next){
		try{
			const about = req.body.newAbout
			const userId = req.body.id
			const currGroup = req.body.group
			const request = await groupDAO.changeAbout(about, userId, currGroup)
			res.json("Updated")
		}catch(e){
			console.error(e)
			return {error: e}
		}
	}
}