import express from "express"

import UsersCtrl from "./users.controller.js"
import GroupCtrl from "./GroupController.js"

// import ReviewsCtrl from "./reviews.controller.js"

const router = express.Router()

router.route("/").get(UsersCtrl.apiGetUsers)
router.route("/").post(UsersCtrl.apiPostUser)
router.route("/").put(UsersCtrl.apiUpdateUser)
router.route("/Groups").get(GroupCtrl.apiGetGroups)
router.route("/MakeGroup").post(GroupCtrl.apiPostGroup)
router.route("/GCal").put(GroupCtrl.apiUpdateCal)
router.route("/GroupAbout").put(GroupCtrl.apiUpdateAbout)
router.route("/addInvites").put(GroupCtrl.apiAddInvite)
router.route("/accInvite").put(GroupCtrl.apiAccInvite)
router.route("/denyInvite").put(GroupCtrl.apiDenyInvite)
// router.route("/").get(RestaurantsCtrl.apiGetRestaurants)
// router.route("/id/:id").get(RestaurantsCtrl.apiGetRestaurantById)
// router.route("/cuisines").get(RestaurantsCtrl.apiGetRestaurantCuisines)

//router
//   .route("/review")
//   .post(ReviewsCtrl.apiPostReview)
//   .put(ReviewsCtrl.apiUpdateReview)
//   .delete(ReviewsCtrl.apiDeleteReview)

export default router